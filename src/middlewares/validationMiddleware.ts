import * as yup from 'yup';
import { Request, Response, NextFunction } from 'express';

export async function validateBook(req: Request, res:Response, next: NextFunction) {
  const bookInfo = req.body;
  let schema = yup.object().shape({
    titulo: yup.string().required(),
    editora: yup.string().required(),
    foto: yup.string().url().required(),
    autores: yup.string().required(),
  });

  const validity = await schema.isValid(bookInfo).then((valid) => valid)

  if (validity === true) {
    next()
  } else {
    res.send({ Erro: 'Todos os campos devem ser preenchidos!' });
  }
}
