import { Router, Request, Response } from 'express';
import LibraryController from './controllers/libraryController';
import { validateBook } from './middlewares/validationMiddleware';

const routes = Router();

routes.post('/obras',validateBook, LibraryController.createRecord)
routes.get('/obras', LibraryController.buscarObras)
routes.put('/obras/:id', LibraryController.atualizarObra)
routes.delete('/obras/:id', LibraryController.apagarRegistro)

export default routes;