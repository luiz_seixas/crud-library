import { Response } from 'express';

export class InternalError extends Error {
  constructor (public message: string, protected code: number = 500) {
    super(message);
  }
}

export function errorHandler(err:any, res: Response) {
  const code = err.code ?? 500;
  const message = err.message ?? "Algo deu errado";

  return res.status(code).json({
    message,
    code,
  });
}