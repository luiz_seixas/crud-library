export interface IBookCreate {
  titulo: string,
  editora: string,
  foto: string,
  autores: string[],
}

export interface IBookUpdate {
  titulo?: string,
  editora?: string,
  foto?: string,
  autores?: string[],
}