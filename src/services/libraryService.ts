import LibraryModel from '../db/entities/library';
import { IBookCreate, IBookUpdate } from './interfaces';
import { Error } from 'mongoose';
import { InternalError } from '../util/errors/handleError';

export class LibraryService {

  public async createRecord(bookInfo: IBookCreate) {
    try {
        const record = new LibraryModel(bookInfo);
        await record.save(record);
        return { message: 'Obra registrada com sucesso!', obra: bookInfo };

    } catch (error: any) {
      throw new InternalError(error.message, error.code)
    }
  }

  public async buscarObras() {
    try {
      const record = await LibraryModel.find({ isDeleted: 0 }, { isDeleted: 0, __v: 0 });
      return record;
    } catch (error: any) {
      throw new InternalError(error.message, error.code)
    }

  }

  public async atualizarObra(id: string, obra: IBookUpdate) {
    try {
      await LibraryModel.updateOne({ _id: id }, obra )
      const atualizado = await LibraryModel.findById(id);
      return { message: 'Obra atualizada com sucesso!', obra: atualizado };
    } catch (error: any) {
      throw new InternalError(error.message, error.code)
    }
  }

  public async apagarRegistro(id: string) {
    try {
      await LibraryModel.updateOne({ _id: id }, { isDeleted: 1});
      return ({ message: 'Registro apagado!' })
    } catch (error: any) {
      throw new InternalError(error.message, error.code)
    }
  }
}