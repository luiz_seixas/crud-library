import { LibraryService } from '../services/libraryService';
import { Response, Request } from 'express'
import { IRecord } from './interfaces';
import { errorHandler } from '../util/errors/handleError';

class LibraryController {
  public async createRecord(req: Request, res: Response) {
    try {
      const body = req.body;
      const service = new LibraryService();
      const result = await service.createRecord(body);
      return res.json(result);
    } catch (error) {
      errorHandler(error, res);
    }
  }

  public async buscarObras(req: Request, res: Response) {
    try {
      const service = new LibraryService();
      const record: IRecord[] = await service.buscarObras();
      return res.send(record);
    } catch (error) {
      errorHandler(error, res);
    }
  }

  public async atualizarObra(req: Request, res: Response) {
    try {
      const service = new LibraryService();
      const { id } = req.params;
      const obra = req.body;
      const record = await service.atualizarObra(id, obra);
      return res.send(record);
    } catch (error) {
      errorHandler(error, res);
    }
  }

  public async apagarRegistro(req: Request, res: Response) {
    try {
      const service = new LibraryService();
      const { id } = req.params;
      await service.apagarRegistro(id)
      res.send({ message: 'Registro apagado!' })
    } catch (error) {
      errorHandler(error, res);
    }
  }
}

export default new LibraryController();