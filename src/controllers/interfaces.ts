export interface IRecord {
  message: string;
  obra: IObra;
}

interface IObra {
  _id: string;
  titulo: string;
  editora: string;
  foto: string;
  autores: string[];
  isDeleted: boolean;
}