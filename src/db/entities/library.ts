import mongoose from  'mongoose';

const LibrarySchema = new mongoose.Schema({
  titulo: {
    type: String,
    required: true,
  },
  editora: {
    type: String,
    required: true,
  },
  foto: {
    type: String,
    required: true,
  },
  autores: {
    type: [String],
    required: true,
  },
  isDeleted: {
    type: Boolean,
    default: 0,
    required: false,
  }
});

const LibraryModel = mongoose.model("Library", LibrarySchema);

export default LibraryModel;

