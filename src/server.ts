import 'reflect-metadata';
import express from 'express';
import helmet from 'helmet';
import cors from 'cors';

import './db/connection';
import routes from './routes';

const app = express();

app.use(helmet());
app.use(express.json());
app.use(routes);
app.use(cors());

const PORT = 3333;

app.listen(PORT, () => console.log(`Server is running on port ${PORT} 🚀 `));